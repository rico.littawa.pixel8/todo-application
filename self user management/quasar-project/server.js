const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const corsOptions = {
  origin: "http://localhost:8080",
};
app.use(cors(corsOptions));
app.use(bodyParser.json());

let users = [
  {
    id: 1,
    name: "Leanne Graham",
    nickname: "graham balls",
    email: "graham@gmail.com",
    contact: "09298289932",
    quote: "Time is gold",
  },
];

// GET all users
app.get("/users", (req, res) => {
  res.json(users);
});

app.get("/users/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const user = users.find((u) => u.id === id);
  if (!user) {
    res.status(404).json({ error: "User not found" });
  } else {
    res.json(user);
  }
});

// POST a new user
app.post("/users", (req, res) => {
  const user = req.body;
  user.id = users.length + 1;
  users.push(user);
  res.json(user);
});

// PUT (update) an existing user
app.put("/users/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const user = users.find((u) => u.id === id);
  if (!user) {
    res.status(404).json({ error: "User not found" });
  } else {
    Object.assign(user, req.body);
    res.json(user);
  }
});

// DELETE a user
app.delete("/users/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const userIndex = users.findIndex((u) => u.id === id);
  if (userIndex === -1) {
    res.status(404).json({ error: "User not found" });
  } else {
    users.splice(userIndex, 1);
    res.json({ message: "User deleted" });
  }
});
// start the server
app.listen(3000, () => {
  console.log("Server started on port 3000");
});
