import { ref } from "vue";
import axios from "axios";

//rows and columns
let rows = ref([]);
let selectedRow = ref({});
let columns = ref([
  {
    name: "name",
    align: "left",
    label: "Name",
    field: "name",
  },
  {
    name: "nickname",
    align: "left",
    label: "Nickname",
    field: "nickname",
  },
  {
    name: "email",
    align: "left",
    label: "Email",
    field: "email",
  },
  {
    name: "contact",
    align: "left",
    label: "Contact",
    field: "contact",
  },
  {
    name: "quote",
    align: "left",
    label: "Quote",
    field: "quote",
  },
  {
    name: "action",
    align: "left",
    label: "Action",
    field: "action",
  },
]);
//get users

const getUsers = async () => {
  try {
    const response = await axios.get("http://localhost:3000/users");
    rows.value = response.data;
  } catch (error) {
    console.error(error);
  }
};

let form = ref({
  name: null,
  nickname: null,
  email: null,
  contact: null,
  quote: null,
});
getUsers();
export { rows, columns, form, selectedRow };
