//Note: run node server.js first

import { ref } from "vue";
import axios from "axios";
let rows = ref([]);

let columns = ref([
  {
    name: "name",
    label: "Name",
    field: "name",
    align: "left",
    sortable: true,
  },
  {
    name: "username",
    label: "Username",
    field: "username",
    align: "left",
    sortable: true,
  },
  {
    name: "email",
    label: "Email",
    field: "email",
    align: "left",
    sortable: true,
  },
  {
    name: "street",
    label: "Street",
    field: "street",
    align: "left",
    sortable: true,
  },
  {
    name: "suite",
    label: "Suite",
    field: "suite",
    align: "left",
    sortable: true,
  },
  {
    name: "city",
    label: "City",
    field: "city",
    align: "left",
    sortable: true,
  },
  {
    name: "zipcode",
    label: "Zipcode",
    field: "zipcode",
    align: "left",
    sortable: true,
  },
  {
    name: "lat",
    label: "LAT",
    field: "lat",
    align: "left",
    sortable: true,
  },
  {
    name: "lng",
    label: "LNG",
    field: "lng",
    align: "left",
    sortable: true,
  },
  {
    name: "phone",
    label: "Phone",
    field: "phone",
    align: "left",
    sortable: true,
  },
  {
    name: "website",
    label: "Website",
    field: "website",
    align: "left",
    sortable: true,
  },
  {
    name: "company",
    label: "Company",
    field: "company",
    align: "left",
    sortable: true,
  },
  {
    name: "catchPhrase",
    label: "Catchphrase",
    field: "catchPhrase",
    align: "left",
    sortable: true,
  },
  {
    name: "bs",
    label: "BS",
    field: "bs",
    align: "left",
    sortable: true,
  },
  {
    name: "action",
    required: true,
    label: "Action",
    align: "left",
    field: "action",
    sortable: true,
  },
]);
//get users from server
const getUsers = async () => {
  try {
    const response = await axios.get("http://localhost:3000/users");
    rows.value = response.data;
  } catch (error) {
    console.error(error);
  }
};
getUsers();

//form variables

let form = ref({
  name: null,
  username: null,
  email: null,
  street: null,
  suite: null,
  city: null,
  zipcode: null,
  lat: null,
  lng: null,
  phone: null,
  website: null,
  company: null,
  catchPhrase: null,
  bs: null,
});

//get selected rows
let selectedRow = ref({});

export { columns, rows, form, selectedRow };
