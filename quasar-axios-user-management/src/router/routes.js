const routes = [
  {
    path: "/",
    redirect: {
      name: "add-user",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "add-user",
        name: "add-user",
        component: () => import("pages/AddUserPage.vue"),
      },
      {
        path: "list-user",
        name: "list-user",
        component: () => import("pages/ListUserPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
