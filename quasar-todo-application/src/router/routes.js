
const routes = [
  {
    path: '/',
    redirect:{
      name:'my-task',
    },
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'my-task',
      name:'my-task',
       component: () => import('pages/MyTask.vue') },
       { path: 'finished-task',
       name:'finished-task',
        component: () => import('pages/FinishedTask.vue') },
        { path: 'deleted-task',
        name:'deleted-task',
         component: () => import('pages/DeletedTask.vue') }
    ]
  },
 

]

export default routes
