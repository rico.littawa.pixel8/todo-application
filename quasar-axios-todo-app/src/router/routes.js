const routes = [
  {
    path: "/",
    redirect: {
      name: "index-page",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "index-page",
        name: "index-page",
        component: () => import("pages/IndexPage.vue"),
      },
      {
        path: "pokemon-data",
        name: "pokemon-data",
        component: () => import("pages/PokemonPage.vue"),
      },
    ],
  },
];

export default routes;
